module bf2go-peg

go 1.12

require (
	github.com/dave/jennifer v1.3.0
	github.com/eiannone/keyboard v0.0.0-20190314115158-7169d0afeb4f
	golang.org/x/sys v0.0.0-20190402142545-baf5eb976a8c // indirect
)
