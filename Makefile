all: bf2go-peg
	mkdir -p test
	./bf2go-peg programs/helloworld.bf test/main.go 
	cd test; go build && ./test


bf2go-peg: main.go bf.peg.go codegen.go
	go build

bf.peg.go: bf.peg
	peg bf.peg

