# bf2go-peg

Brainfuck to go converter using PEG (Parsing Expression Grammar)

Experimental project to explore parser-/ and code generation in golang.


This project currently employs some optimization i came up with myself,
the plan is to include more optimizations of this great post: http://calmerthanyouare.org/2015/01/07/optimizing-brainfuck.html.
As of this writing the produced code for mandelbrot is (on my system at least, when compiled with g1.12.1) about twice as fast as the one produced
by the [optimizing sed compiler](https://github.com/stedolan/bf.sed).

## Requirements
- working golang environment

## Building
Run `make`, which will generate the binary. This will also
build and run the helloworld binary.

## Usage
see `Makefile` for an example.

## Brainfuck source (`programs/`)
- `helloworld.bf` - [wikipedia](https://de.wikipedia.org/wiki/Brainfuck)
- `sierpinski.bf` - [wikipedia](http://es.wikipedia.org/wiki/Brainfuck)
- `mandelbrot.bf` - http://esoteric.sange.fi/brainfuck/bf-source/prog/mandelbrot.b
- `primes.bf` - https://github.com/pablojorge/brainfuck


## libs
- jennifer (code generator): https://github.com/dave/jennifer

## tools:
- peg (parser generator): https://github.com/pointlander/pego

## planned comparisons
- https://github.com/icholy/brainfuck
