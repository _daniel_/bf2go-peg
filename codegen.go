package main

import (
	"github.com/dave/jennifer/jen"
)

type Prog struct {
	nodes []*BFNode
	loops []*BFNode
	stdin bool
}

const (
	TIncPtr = iota
	TDecPtr
	TIncVal
	TDecVal
	TGetChr
	TPutChr
	TLoopBegin
	TLoopEnd
	TSetZero
	TAddMul
)

type BFNode struct {
	Type  int
	Val   int
	Mul   int
	Nodes []*BFNode
}

func (p *Prog) AddBF(nodeType int, val int) {
	//log.Println("nodes:", len(p.nodes))
	//log.Println("addNode", nodeType, val)
	node := BFNode{
		Type: nodeType,
		Val:  val,
		Mul:  0,
	}
	if nodeType == TLoopEnd {
		//log.Println("ending loop", len(p.loops))
		p.loops = p.loops[0 : len(p.loops)-1]
		return
	}
	if len(p.loops) == 0 {
		p.nodes = append(p.nodes, &node)
	} else {
		p.loops[len(p.loops)-1].Nodes = append(p.loops[len(p.loops)-1].Nodes, &node)
	}
	if nodeType == TGetChr {
		p.stdin = true
	}
	if nodeType == TLoopBegin {
		//log.Println("beginning loop")
		p.loops = append(p.loops, &node)
	}
}
func (p *Prog) generateHead() []jen.Code {
	inDecl := jen.Null()
	if p.stdin {
		inDecl = jen.Id("input").Op(":=").Qual("bufio", "NewReader").Call(jen.Qual("os", "Stdin"))
	}
	return []jen.Code{
		jen.Id("band").Op(":=").Index(jen.Lit(30000)).Byte().Values(),
		jen.Var().Id("pointer").Int(),
		inDecl}
}
func (p *Prog) Generate(path string) error {
	f := jen.NewFile("main")

	code :=
		append(p.generateHead(), nodesToStatements(p.nodes)...,
		)
	f.Func().Id("main").Params().Block(code...)
	//log.Printf("%#v", f)
	return f.Save(path)
}

func nodesToStatements(nodes []*BFNode) []jen.Code {
	//	result := make([]jen.Code, len(nodes))
	result := []jen.Code{}
	for _, n := range nodes {
		//result[i] = n.toCode()
		result = append(result, n.toCode()...)
	}
	return result
}

func band() jen.Code {
	return jen.Id("band").Index(jen.Id("pointer"))
}

func getChar() []jen.Code {
	return []jen.Code{
		jen.List(band(), jen.Id("_")).Op("=").Id("input").Dot("ReadByte").Call(),
	}
}

func (n *BFNode) toCode() []jen.Code {
	//log.Println(n.Type)
	switch n.Type {
	case TLoopBegin:
		return []jen.Code{jen.For(jen.Id("band").Index(jen.Id("pointer")).Op("!=").Lit(0)).Block(nodesToStatements(n.Nodes)...)}
	case TIncPtr:
		return []jen.Code{jen.Id("pointer").Op("+=").Lit(n.Val)}
	case TDecPtr:
		return []jen.Code{jen.Id("pointer").Op("-=").Lit(n.Val)}
	case TIncVal:
		return []jen.Code{jen.Id("band").Index(jen.Id("pointer")).Op("+=").Lit(n.Val)}
	case TDecVal:
		return []jen.Code{jen.Id("band").Index(jen.Id("pointer")).Op("-=").Lit(n.Val)}
	case TPutChr:
		return []jen.Code{jen.Qual("fmt", "Print").Call(jen.String().Call(jen.Id("band").Index(jen.Id("pointer"))))}
	case TSetZero:
		return []jen.Code{jen.Id("band").Index(jen.Id("pointer")).Op("=").Lit(0)}
	case TGetChr:
		return getChar()
	default:
		panic("aaaaa")
	}
}
