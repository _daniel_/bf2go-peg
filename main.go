package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"os"
)

func main() {
	if len(os.Args) == 1 {
		log.Println("error: no brainfuck source given as argument")
		printUsage()
		os.Exit(1)
	} else if len(os.Args) == 2 {
		log.Println("error: no output path given as argument")
		printUsage()
		os.Exit(1)
	}

	buffer, err := ioutil.ReadFile(os.Args[1])
	if err != nil {
		panic(err)
	}

	bf := &BF2Go{
		Buffer: string(buffer),
	}
	bf.Init()

	err = bf.Parse()
	if err != nil {
		panic(err.Error())
	}

	bf.Execute()

	err = bf.Generate(os.Args[2])
	if err != nil {
		panic(err.Error())
	}
}

func printUsage() {
	fmt.Printf("usage:\n"+
		"	%s <in.bf> <out.go>\n", os.Args[0])
}
